const jwt = require("jsonwebtoken");
require("dotenv").config();

//this middleware will on continue on if the token is inside the local storage

module.exports = function (req, res, next) {
  // Get token from header
  console.log("user authentication using token")
  try {
    const jwttoken = req.header("token");
    console.log(jwttoken);

    // Check if not token
    if (!jwttoken) {
      return res.status(403).json({ msg: "Not Authorize Hey" });
    }
    const payload = jwt.verify(jwttoken, process.env.jwtSecret);
    console.log("user id ",payload.user);
    req.user = payload.user;
  } catch (err) {
    // Verify token
    console.error(err.message);
    res.status(401).json("Not Authorize hello");
  }
  next();
};
