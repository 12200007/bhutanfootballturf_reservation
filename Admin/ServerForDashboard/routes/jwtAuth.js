const router = require("express").Router();
const pool = require("../db");
const jwtGenerator = require("../utils/jwtGenerator");
const bcrypt = require("bcrypt");
const authorization = require("../middleware/authorization");

const validInfo = require("../middleware/validInfo");

router.post("/register", validInfo, async (req, res) => {
  // console.log("m running");
  try {
    const { name, email, password } = req.body;
    const user = await pool.query("select * from users where user_email=$1", [
      email,
    ]);

    if (user.rows.length !== 0) {
      return res.status(401).send("user already exists");
    }

    const saltRound = 10;
    const salt = await bcrypt.genSalt(saltRound);
    const bcryptPassword = await bcrypt.hash(password, salt);

    const newUser = await pool.query(
      "insert into users (user_name,user_email,user_password) values ($1,$2,$3) returning user_id",
      [name, email, bcryptPassword]
    );
    const userId = newUser.rows[0].user_id;
    const token = jwtGenerator(userId);
    res.json({ token: token });
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

router.post("/login", validInfo, async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await pool.query("SELECT * FROM users WHERE user_email = $1", [
      email,
    ]);

    if (user.rows.length === 0) {
      return res.status(401).json("Password or Email is in correct ");
    }

    const validPassword = await bcrypt.compare(
      password,
      user.rows[0].user_password
    );

    if (!validPassword) {
      return res.status(401).json("Password or Email is in correct");
    }
    const token = jwtGenerator(user.rows[0].user_id);
    return res.json({ token, "user_id": user.rows[0].user_id });
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server error");
  }
});

router.get("/verify",authorization, (req, res) => {
  try {
    console.log("sending user is valid")
    res.json(true);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server error");
  }
});

module.exports = router;

















// const express = require("express");
// const router = express.Router();
// const bcrypt = require("bcrypt");
// const pool = require("../db");
// const validInfo = require("../middleware/validInfo");
// const authorize = require("../middleware/authorize");

// //authorizeentication
// const test = async () => {
//   console.log("trying to connect")
//   await pool.query('SELECT NOW()', (err, res) => {
//     if (err) {
//       console.log('Error connecting to the database: ', err.stack);
//     } else {
//       console.log('Successfully connected to the database at: ', res.rows[0].now);
//     }
//   });
//   console.log("finished")
// }

// test();

// router.post("/register", validInfo, async (req, res) => {
//   const { email, name, password } = req.body;
//   console.log(email)

//   try {
//     console.log("getting user")
//     const user = await pool.query('SELECT * FROM users WHERE user_email = $1', [
//       email
//     ]);

//     console.log(user.rows)

//     if (user.rows.length > 0) {
//       return res.status(401).json("User already exist!");
//     }

//     const salt = await bcrypt.genSalt(10);
//     const bcryptPassword = await bcrypt.hash(password, salt);

//     let newUser = await pool.query(
//       "INSERT INTO users (user_name, user_email, user_password) VALUES ($1, $2, $3) RETURNING *",
//       [name, email, bcryptPassword]
//     );

//     res.json({jwtToken});
//   } catch (err) {
//     console.log(err.message);
//     res.status(500).send("Server error");
//   }
// });

// router.post("/login", validInfo, async (req, res) => {
//   const { email, password } = req.body;

//   try {
//     const user = await pool.query("SELECT * FROM users WHERE user_email = $1", [
//       email
//     ]);

//     if (user.rows.length === 0) {
//       return res.status(401).json("Invalid Credential");
//     }

//     const validPassword = await bcrypt.compare(
//       password,
//       user.rows[0].user_password
//     );

//     if (!validPassword) {
//       return res.status(401).json("Invalid Credential");
//     }
//     const jwtToken = jwtGenerator(user.rows[0].user_id);
//     return res.json({ jwtToken });
//   } catch (err) {
//     console.error(err.message);
//     res.status(500).send("Server error");
//   }
// });

// router.post("/verify", authorize, (req, res) => {
//   try {
//     res.json(true);
//   } catch (err) {
//     console.error(err.message);
//     res.status(500).send("Server error");
//   }
// });

// module.exports = router;
