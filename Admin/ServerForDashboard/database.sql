CREATE DATABASE store;

CREATE TABLE users(
  user_id uuid PRIMARY KEY DEFAULT 
  uuid_generate_v4(),
  user_name VARCHAR(255) NOT NULL,
  user_email VARCHAR(255) NOT NULL,
  user_password VARCHAR(255) NOT NULL
);

CREATE TABLE todo(
  todo_id SERIAL,
  user_id UUID ,
  description VARCHAR(255),
  PRIMARY KEY (todo_id),
  FOREIGN KEY (user_id) REFERENCES users(user_id)
  );


CREATE TABLE user_profile (
  id SERIAL PRIMARY KEY,
  profile_picture VARCHAR(255)
);


INSERT INTO users (user_name, user_email, user_password) VALUES ('henry',
'henryly213@gmail.com', 'kthl8822');

--  INSERT INTO users (user_name, user_email, user_password) VALUES ('Jigme',
--  'Jigme213@gmail.com', '1234');
 