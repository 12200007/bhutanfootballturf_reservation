const express = require("express");
const app = express();
const cors = require("cors");
const pool = require("./db");
const bcrypt = require("bcrypt");
const nodemailer = require("nodemailer");
const router = express.Router();
app.use(express.json());
app.use(cors());

function diff_minutes(dt2, dt1) {
  const diff = dt2.getTime() - dt1.getTime();

  return diff / 60000;
}

// const upload = multer({ dest: 'uploads/' });

// app.put('/api/users/:id/profile-picture', upload.single('profilePicture'), async (req, res) => {
//   try {
//     const { id } = req.params;
//     const { path } = req.file;

//     const updateUser = await pool.query(
//       'UPDATE user_profile SET profile_picture = $1 WHERE id = $2',
//       [path, id]
//     );

//     res.json('User profile picture updated successfully');
//   } catch (err) {
//     console.error(err.message);
//     res.status(500).json('Server Error');
//   }
// });

app.use("/authentication", require("./routes/jwtAuth"));

app.use("/dashboard", require("./routes/dashboard"));

app.get("/turf-reservations", async (req, res) => {
  try {
    const resrvTb = await pool.query("SELECT * FROM turf_reservations");

    res.status(200).json(resrvTb.rows);
  } catch (err) {
    res.status(500).send(`Failed to get reservations! ${err.message}`);
  }
});

app.delete("/turf-reservation", async (req, res) => {
  const { resrvId } = req.body;

  try {
    await pool.query(
      "DELETE FROM turf_reservations\
      WHERE resrv_id = $1",
      [resrvId]
    );

    res.status(200).send("cancel success!");
  } catch (err) {
    res.status(500).send(`Failed to delete reservation! ${err.message}`);
  }
});

app.get("/feedback", async (req, res) => {
  try {
    // Retrieve feedback from the database
    const query = "SELECT * FROM feedback";
    const result = await pool.query(query);

    // Send the retrieved feedback as the response
    res.status(200).json(result.rows);
  } catch (error) {
    console.error("Error retrieving feedback:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

app.delete("/feedback/:id", async (req, res) => {
  const { id } = req.params;

  try {
    // Delete the feedback with the specified ID from the database
    const query = "DELETE FROM feedback WHERE id = $1";
    await pool.query(query, [id]);

    res.status(200).json({ message: "Feedback deleted successfully" });
  } catch (error) {
    console.error("Error deleting feedback:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

app.post("/users/update", async (req, res) => {
  try {
    const { email, oldPassword, newPassword, confirmPassword } = req.body;

    //user query
    const user = await pool.query("SELECT * FROM users WHERE user_email = $1", [
      email,
    ]);

    // check if current password is valid
    const passwordValid = await bcrypt.compare(
      oldPassword,
      user.rows[0].user_password
    );

    console.log("old password ", oldPassword)
    console.log("hash ", user.rows[0].user_password)
    console.log("compare result ", passwordValid)

    if (!passwordValid) {
      return res.status(401).json("Incorrect password.");
    }
    //check if the new and confirm password is same
    if (newPassword !== confirmPassword) {
      return res.status(401).json("Passwords do not match!");
    }

    const saltRound = 10;
    const salt = await bcrypt.genSalt(saltRound);
    const bcryptPassword = await bcrypt.hash(newPassword, salt);

    const updatedUser = await pool.query(
      "UPDATE users SET user_password = $1 WHERE user_id = $2 RETURNING *",
      [bcryptPassword, user.rows[0].user_id]
    );

    res.status(200).json("Password changed Successful");
  } catch (err) {
    console.error(err.message);
    res.status(500).json("The given Email is not registered");
  }
});



const transporter = nodemailer.createTransport({
  host: "smtp.zoho.com",
  port: 465,
  secure: true,
  auth: {
    user: "bhutanfootballturf@zohomail.com", // Replace with your email address
    pass: "@wangs002", // Replace with your email password
  },
});

// Function to generate OTP
function generateOTP() {
  return Math.floor(1000 + Math.random() * 9000);
}

// Send the OTP email
async function sendOTPEmail(email, otp) {
  try {
    await transporter.sendMail({
      from: "bhutanfootballturf@zohomail.com", // Replace with your email address
      to: email,
      subject: "OTP for Password Reset",
      text: `Your OTP is ${otp}. Please use this OTP to reset your password, and it will expire in 5 minutes.`,
    });
    console.log("OTP email sent successfully");
  } catch (error) {
    console.error("Error sendingOTP email:", error);
  }
}

app.post("/sendOTP", async (req, res) => {
  const { email } = req.body;
  console.log(email)
  const otp = generateOTP();

  try {
    await pool.query("BEGIN");
    try {
      const reqTb = await pool.query(
        "INSERT INTO forgot_password_requests\
      (email, otp)\
      VALUES($1, $2)\
      RETURNING *",
        [email, otp]
      );
      
      await sendOTPEmail(email, otp);
      
      pool.query("COMMIT");

      return res.status(200).json(reqTb.rows[0]);
    } catch (err) {
      return res.status(500).json({ message: "Failed to send OTP" });
    }
  } catch (err) {
    res.status(500).json({ message: "Failed to send OTP" });
  }
});


app.post("/verify-forgot-password-otp", async (req, res) => {
  const { reqId, enteredOtp, newPassword, confirmPassword } = req.body;

  try {
    const reqTb = await pool.query(
      "SELECT * FROM forgot_password_requests\
    WHERE req_id = $1",
      [reqId]
    );

    if (reqTb.rows.length === 0) {
      return res.status(403).send("Forgot password request not found!");
    }

    const {
      email,
      otp,
      doc,
    } = reqTb.rows[0];
    const now = new Date();
    const otpDate = new Date(doc);

    if (newPassword != confirmPassword) {
      return res.status(500).send("Password doesn't match");
    }

    if (parseInt(enteredOtp) !== otp) {
      return res.status(402).send("OTP invalid!");
    }

    if (diff_minutes(now, otpDate) > 5) {
      return res.status(498).send("OTP expired!");
    }

    const userTb = await pool.query(
      "SELECT * FROM forgot_password_requests\
    WHERE email = $1",
      [email]
    );

    if (userTb.rows.length == 0) {
      return res.status(500).send("No request with such email");
    }

    await pool.query("BEGIN");
    try {
      const saltRound = 10;
      const salt = await bcrypt.genSalt(saltRound);
      const bcryptPassword = await bcrypt.hash(newPassword, salt);

      const userTb = await pool.query(
        "UPDATE users SET user_password = $1 WHERE user_email = $2\
        RETURNING *",
        [bcryptPassword, email]
      );

      if (userTb.rows.length == 0) throw new Error("No user with such email");

      pool.query("COMMIT");

      return res.status(200).json(reqTb.rows[0]);
    } catch (err) {
      pool.query("ROLLBACK");

      return res.status(500).send(`Failed to reserve! ${err.message}`);
    }
  } catch (err) {
    res.status(500).send(`Failed to reserve! ${err.message}`);
  }
});


module.exports = router;



app.listen(5001, () => {
  console.log("server is running on port 5001");
});
