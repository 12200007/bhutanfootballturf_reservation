import React, { useEffect, useState } from "react";
import Navbar from "./NavBar";
import Navbar2 from "./Navbar2";
import "./feedback.css";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";

function Feedback({ setAuth }) {
  const [feedbackData, setFeedbackData] = useState([]);

  useEffect(() => {
    fetchFeedbackData();
  }, []);

  const fetchFeedbackData = async () => {
    try {
      const response = await fetch("http://localhost:5001/feedback");
      const data = await response.json();

      setFeedbackData(data.map(item => {
        item.created_at = new Date(item.created_at);
        return item;
      }));
    } catch (error) {
      console.error("Error retrieving feedback:", error);
    }
  };

  const tableHeaderStyle = {
    borderBottom: "1px solid black",
    padding: "8px",
  };

  const tableCellStyle = {
    borderBottom: "1px solid black",
    padding: "8px",
  };

  const handleDeleteFeedback = async (id) => {
    confirmAlert({
      title: "Confirm deletion",
      message: "Are you sure you want to delete this feedback?",
      buttons: [
        {
          label: "Yes",
          onClick: async () => {
            try {
              // Make an API request to delete the feedback
              await fetch(`http://localhost:5001/feedback/${id}`, {
                method: "DELETE",
              });

              // Update the feedbackData state by removing the deleted feedback
              setFeedbackData((prevData) =>
                prevData.filter((item) => item.id !== id)
              );
            } catch (error) {
              console.error("Error deleting feedback:", error);
            }
          },
        },
        {
          label: "No",
          onClick: () => {},
        },
      ],
    });
    
  };
  

  return (
    <>
      <Navbar />
      <div className="feedback-container">
        <Navbar2 setAuth={setAuth} />
        <h1 style={{ marginLeft: "600px", marginTop: "-650px" }}>
          Feedback Dashboard
         
        </h1>
        <span>
          <table
            className="feedback-table"
            style={{ marginLeft: "250px", marginTop: "50px" }}
          >
            <thead>
              <tr>
                <th style={tableHeaderStyle}>Name</th>
                <th style={tableHeaderStyle}>Email</th>
                <th style={tableHeaderStyle}>Phone Number</th>
                <th style={{ ...tableHeaderStyle, width: "100px" }}>Date</th>
                <th style={{ ...tableHeaderStyle, width: "110px" }}>Time</th>
                <th style={{ ...tableHeaderStyle, width: "200px" }}>
                  Feedback
                </th>
                <th style={tableHeaderStyle}>Actions</th>
              </tr>
            </thead>
            <tbody>
              {feedbackData.map((item) => (
                <tr key={item.id}>
                  <td style={tableCellStyle}>{item.name}</td>
                  <td style={tableCellStyle}>{item.email}</td>
                  <td style={tableCellStyle}>{item.phone_number}</td>
                  <td style={{ ...tableCellStyle, width: "160px" }}>
                    {item.created_at.toDateString()}
                  </td>
                  <td style={{ ...tableCellStyle, width: "110px" }}>
                    {item.created_at.toLocaleTimeString('en-US')}
                  </td>
                  <td style={{ ...tableCellStyle, width: "500px" }}>
                    {item.feedback}
                  </td>

                  <td
                    className="delete-cell"
                    style={{
                      ...tableCellStyle,
                      color: "red",
                      cursor: "pointer",
                      fontWeight: "bold",
                      textDecoration: "none",
                    }}
                    onClick={() => handleDeleteFeedback(item.id)}
                    onMouseEnter={(e) => {
                      e.target.style.textDecoration = "underline";
                      e.target.style.cursor = "pointer";
                    }}
                    onMouseLeave={(e) => {
                      e.target.style.textDecoration = "none";
                      e.target.style.cursor = "default";
                    }}
                  >
                    Delete
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </span>
      </div>
    </>
  );
}

export default Feedback;
