import React, { Fragment, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./login.css";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Login = ({ setAuth }) => {
  const navigate = useNavigate();
  console.log(setAuth);

  const [inputs, setInputs] = useState({
    email: "",
    password: "",
  });

  const { email, password } = inputs;

  const onChange = (e) =>
    setInputs({ ...inputs, [e.target.name]: e.target.value });

  const onSubmitForm = async (e) => {
    e.preventDefault();

    try {
      const body = { email, password };
      const response = await fetch(
        "http://localhost:5001/authentication/login",
        {
          method: "POST",
          headers: {
            "Content-type": "application/json",
          },
          body: JSON.stringify(body),
        }
      );

      const parseRes = await response.json();

      if (parseRes.token) {
        localStorage.setItem("token", parseRes.jwtToken);
        setAuth(true);
        toast.success("Logged in Successfully");

        sessionStorage.setItem("user_id", parseRes.user_id);
        navigate("/dashboard");
      } else {
        setAuth(false);
        toast.error(parseRes);

        // toast.success("Password is incorrect");
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  return (
    <Fragment>
      <div className="Logins" style={{ marginTop: "150px" }}>
        <div class="container-fluid">
          <div class="row main-content text-center">
            <div
              class="col-md-4 text-center company__info"
              style={{ background: "#f5f5f5" }}
            >
              <span>
                <h2 style={{ color: "black", marginLeft: "-10px" }}>
                  WELCOME TO ADMIN
                </h2>
              </span>
              <br></br>
              <span>
                <h2
                  style={{
                    color: "black",
                    marginLeft: "-14px",
                    marginTop: "20px",
                    fontSize: "15px",
                    color: "#16BBD9",
                  }}
                >
                  Bhutan Football Turf Reservation
                </h2>
              </span>
            </div>
            <div
              class="col-md-8 col-xs-12 col-sm-12 login_form"
              style={{ background: "#0B7048", width: "400px" }}
            >
              <div class="container-fluid">
                <div class="row">
                  <h2 style={{ color: "white" }}>Log In</h2>
                </div>
                <div class="row">
                  <form control="" class="form-group" onSubmit={onSubmitForm}>
                    <div class="row">
                      <input
                        style={{ marginLeft: "0%", borderRadius: "10px" }}
                        type="text"
                        name="email"
                        value={email}
                        id="email"
                        class="form__input"
                        placeholder="Email"
                        onChange={(e) => onChange(e)}
                      />
                    </div>
                    <div class="row">
                      <span class="fa fa-lock"></span>
                      <input
                        style={{ borderRadius: "10px" }}
                        type="password"
                        name="password"
                        value={password}
                        id="password"
                        class="form__input"
                        placeholder="Password"
                        onChange={(e) => onChange(e)}
                      />
                    </div>

                    <div>
                      <div
                        style={{
                          color: "white",
                          marginLeft: "233px",
                          marginTop: "-25px",
                        }}
                      >
                        <a
                          href="ForgotPassword"
                          style={{ textDecoration: "none", color: "white" }}
                        >
                          Forgot Password
                        </a>
                      </div>
                    </div>

                    <div class="row">
                      <input
                        type="submit"
                        value="Submit"
                        class="btn"
                        style={{
                          width: "100px",
                          marginLeft: "120px",
                          marginTop: "50px",
                          background: "#013220",
                          color: "white",
                        }}
                        onClick={onSubmitForm}
                      />
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />

      <ToastContainer />
    </Fragment>
  );
};

export default Login;
