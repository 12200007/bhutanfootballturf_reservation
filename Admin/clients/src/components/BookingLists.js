import React from "react";
import Navbar from "./NavBar";
import Navbar2 from "./Navbar2";
// import "./Bookinglist.css";
import { useState, useEffect } from "react";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
const BookingLists = (setAuth) => {
  const [data, setData] = useState([]);
  function getReservations() {
    fetch("http://localhost:5001/turf-reservations")
      .then((res) => {
        if (res.ok) {
          res.json().then((json) => {
            setData(json);
          });
        } else {
          res.text().then((text) => {
            alert(text);
          });
        }
      })
      .catch((err) => {
        alert(err.message);
      });
  }
  function cancelReservation(id) {
    confirmAlert({
    title: "Confirm",
    message: "Are you sure you want to cancel this reservation?",
    buttons: [
    {
    label: "Yes",
    onClick: () => {
    fetch("http://localhost:5001/turf-reservation", {
    method: "DELETE",
    headers: {
    "Content-Type": "application/json",
    },
    body: JSON.stringify({ resrvId: id }),
    })
    .then((res) => {
    if (res.ok) getReservations();
    })
    .catch((err) => {
    alert(err.message);
    });
    },
    },
    {
    label: "No",
    onClick: () => {},
    },
    ],
    });
    }
  useEffect(() => {
    getReservations();
  }, []);
  return (
    <>
      <Navbar />

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Navbar2 setAuth={setAuth} />

        <span
          style={{
            marginRight: "700px",
            marginTop: "-600px",
            display: "block",
          }}
        >
          <h1
            style={{
              marginTop: "600px",
              whiteSpace: "nowrap",
              textAlign: "center",
              width: "100%",
            }}
          >
            User Football Booking Lists
           
          </h1>
          <div
            className="bookings"
            style={{ maxHeight: "500px", overflowY: "auto" }}
          >
            <div className="container" style={{ height: "500px" }}>
              <table
                className="table table-bordered"
                style={{ width: "1300px", marginTop: "30px" }}
              >
                <thead style={{ fontSize: "20px" }}>
                  <tr>
                    <th style={{ fontWeight: "bold", width: "20%" }}>Email</th>
                    <th style={{ fontWeight: "bold", width: "16%" }}>Time</th>
                    <th style={{ fontWeight: "bold", width: "20%" }}>
                      Date Booked
                    </th>
                    <th style={{ fontWeight: "bold", width: "15%" }}>
                      Dzongkhag
                    </th>
                    <th style={{ fontWeight: "bold", width: "15%" }}>
                      Phone Number
                    </th>
                    <th style={{ fontWeight: "bold", width: "15%" }}>
                      Journal Number
                    </th>
                    <th style={{ fontWeight: "bold" }}>Action</th>
                  </tr>
                </thead>
                <tbody style={{ fontSize: "15px" }}>
                  {data.map((item) => {
                    return (
                      <tr key={item.resrv_id}>
                        <td>{item.resrv_email}</td>
                        <td>{item.time}</td>
                        <td>{new Date(item.date).toLocaleDateString()}</td>
                        <td>{item.dzongkhag}</td>
                        <td>{item.resrv_phone_number}</td>
                        <td>{item.jrl_no}</td>
                        <td
                          style={{
                            color: "red",
                            cursor: "default",
                          }}
                          onClick={() => cancelReservation(item.resrv_id)}
                          onMouseEnter={(e) => {
                            e.target.style.fontWeight = "bold";
                            e.target.style.cursor = "default";
                          }}
                          onMouseLeave={(e) => {
                            e.target.style.fontWeight = "normal";
                            e.target.style.cursor = "default";
                          }}
                        >
                          Cancel
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </span>
      </div>
    </>
  );
};

export default BookingLists;
