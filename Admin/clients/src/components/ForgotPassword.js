import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import "./OTPForgotPassword.css";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./login";

const ForgotPasswordForm = () => {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await fetch("http://localhost:5001/sendOTP", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email }),
      });

      if (res.ok) {
        res.json().then((json) => {
          toast.success("Check your email for verification");
          navigate(`/OTPForgotPassword/${json.req_id}`);
        });
      } else {
        res.json().then((json) => toast.error(JSON.stringify(json)));
      }
      // Handle successful response or display a success message to the user
    } catch (error) {
      console.error(error);
      // Handle error or display an error message to the user
    }
  };

  const handleBack = () => {
    window.history.back();
  };

  return (
    <>
      <div className="center-container">
        <div className="forgot-password-form-container">
          <h2 style={{ textAlign: "center" }}>Forgot Password</h2>
          <div className="close-button" onClick={handleBack}>
            <span>&times;</span>
          </div>
          <form onSubmit={handleSubmit}>
            <label htmlFor="email">Email:</label>
            <input
              type="email"
              id="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
            <button type="submit">Submit</button>
          </form>

          <style jsx>{`
           
            .forgot-password-form-container {
              max-width: 400px;
              margin: 0 auto;
              padding: 20px;
              border: 1px solid #ccc;
              box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
            }

            h2 {
              font-size: 1.5rem;
              margin-bottom: 20px;
            }

            form {
              display: flex;
              flex-direction: column;
            }

            label {
              margin-bottom: 5px;
            }

            input {
              margin-bottom: 10px;
              padding: 8px;
              border: 1px solid #ccc;
              border-radius: 4px;
            }

            button {
              padding: 8px 12px;
              background-color: #007bff;
              color: #fff;
              border: none;
              border-radius: 4px;
              cursor: pointer;
            }
          `}</style>
          <ToastContainer position="top-right" />
        </div>
      </div>
    </>
  );
};

export default ForgotPasswordForm;
