import React, { useState } from "react";
import { Link } from "react-router-dom";
import Navbar from "./NavBar";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Navbar2 from "./Navbar2";

const EditProfile = ({ setAuth }) => {
  const [userId, setUserId] = useState(sessionStorage.getItem("user_id"));
  const [email, setEmail] = useState("");
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handleOldPasswordChange = (e) => {
    setOldPassword(e.target.value);
  };

  const handleNewPasswordChange = (e) => {
    setNewPassword(e.target.value);
  };

  const handleConfirmPasswordChange = (e) => {
    setConfirmPassword(e.target.value);
  };

  const validatePassword = (password) => {
    const regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    return regex.test(password);
  };

  async function handleSaveChanges(e) {
    e.preventDefault();

    if (!validatePassword(newPassword)) {
      toast.error(
        "Password must contain at least one number, one uppercase letter, one lowercase letter, and be at least 8 characters long"
      );
      return;
    }

    try {
      const body = {
        email,
        oldPassword,
        newPassword,
        confirmPassword,
      };

      const response = await fetch(`http://localhost:5001/users/update`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(body),
      });
      const parseResponse = await response.json();

      if (response.status === 200) {
        toast.success(parseResponse);
      } else {
        toast.error(parseResponse);
      }
    } catch (err) {
      console.error(err.message);
    }
  }
  return (
    <>
      <Navbar />
      <div className="profile">
        <div>
          <div className="modal-dialog">
            <div className="modal-content">
              <Navbar2 setAuth={setAuth} />

              <div
                className="modal-body"
                style={{ marginLeft: "25%", marginTop: "-650px" }}
              >
                <h1 style={{ marginLeft: "250px", marginTop: "-10px" }}>
                  Change Password
                </h1>
                <div className="form-group">
                  <label>Enter Email</label>
                  <input
                    style={{ width: "60%" }}
                    type="email"
                    className="form-control"
                    name="email"
                    value={email}
                    onChange={handleEmailChange}
                  />
                </div>
                <br></br>

                <div className="form-group">
                  <label>Enter Old Password</label>
                  <input
                    style={{ width: "60%" }}
                    type="password"
                    className="form-control"
                    name="oldPassword"
                    value={oldPassword}
                    onChange={handleOldPasswordChange}
                  />
                </div>
                <br></br>
                <div className="form-group">
                  <label>Enter New Password</label>
                  <input
                    style={{ width: "60%" }}
                    type="password"
                    className="form-control"
                    name="newPassword"
                    value={newPassword}
                    onChange={handleNewPasswordChange}
                  />
                </div>
                <br></br>
                <div className="form-group">
                  <label>Confirm New Password</label>
                  <input
                    style={{ width: "60%" }}
                    type="password"
                    className="form-control"
                    name="confirmPassword"
                    value={confirmPassword}
                    onChange={handleConfirmPasswordChange}
                  />
                </div>
                <br></br>

                <div
                  style={{
                    marginRight: 180,
                    class: "modal-footer",
                    marginTop: "0%",
                    width: "250px",
                    marginLeft: "300px",
                  }}
                >
                  {/* <Link type="submit" className="btn btn-success">
                    Update
                  </Link> */}
                  <button
                    className="btn btn-success"
                    onClick={handleSaveChanges}
                  >
                    {" "}
                    Update
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="light"
        />
        <ToastContainer />
      </div>
    </>
  );
};

export default EditProfile;
