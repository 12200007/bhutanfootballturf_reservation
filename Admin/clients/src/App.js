import { React } from "react";
import { useState, useEffect} from "react";
import BookingLists from "./components/BookingLists";
import Feedback from "./components/Feedback";
import Navbar from "./components/NavBar";
import Navbar2 from "./components/Navbar2";
import EditProfile from "./components/editProfile";
import ForgotPassword from "./components/ForgotPassword";
import OTPForgotPassword from "./components/OTPForgotPassword";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
} from "react-router-dom";
import Login from "./components/login";

function Dashboard({setAuth}) {
  return (
    <>
      <Navbar />
      <Navbar2 setAuth={setAuth}/>
      <h1 style={{marginTop:"-650px",marginLeft:"200px",textAlign:"center"}}>
        Welcome Back To Admin dashboard
      </h1>
    </>
  );
}

function App() {
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  console.log(isAuthenticated);

  const setAuth = (boolean) => {
    setIsAuthenticated(boolean);
  };
  const checkAuthenticated = async () => {
    try {
      const res = await fetch("http://localhost:5001/authentication/verify", {
        method: "GET",
        headers: { jwt_token: localStorage.token },
      });

      const parseRes = await res.json();

      parseRes === true ? setIsAuthenticated(true) : setIsAuthenticated(false);
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    checkAuthenticated();
  }, []);
  return (
    <Router>
      <Routes>
        <Route
          path="/"
          element={
            !isAuthenticated ? <Login setAuth={setAuth} /> :( <Navigate to="/" />)
          }
        />

        <Route
          path="/dashboard"
          element={
            isAuthenticated ? <Dashboard setAuth={setAuth} /> : (<Navigate to="/" />)
          }
        />
        <Route
          path="/feedback"
          element={
            isAuthenticated ? <Feedback setAuth={setAuth} /> : (<Navigate to="/" />)
          }
        />
        <Route
          path="/bookinglists"
          element={
            isAuthenticated ? <BookingLists setAuth={setAuth} /> : (<Navigate to="/" />)
          }
        />
        <Route
          path="/editProfile"
          element={
            isAuthenticated ? <EditProfile setAuth={setAuth} /> : (<Navigate to="/" />)
          }
        />
        <Route exact path="/ForgotPassword" element={<ForgotPassword />} />
        <Route exact path="/OTPForgotPassword/:reqId" element={<OTPForgotPassword />} />
       
      </Routes>
      
    </Router>
  );
}

export default App;
