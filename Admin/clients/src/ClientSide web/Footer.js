import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";

import {
  MDBFooter,
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBIcon,
} from "mdb-react-ui-kit";

function Footer() {
  return (
    <div>
      <MDBFooter
        bgColor="light"
        className="footerstyle text-center text-lg-start text-muted"
        style={{ boxShadow: "0 0 10px rgba(0,0,0,0.2)", marginTop: "100px" }}
      >
        <section className="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">
          <div className="me-5 d-none d-lg-block">
            <span>Get connected with us on social networks:</span>
          </div>

          <div>
            <a
              href="https://www.facebook.com/search/top?q=bhutan%20football%20federation"
              className="me-4 text-reset"
            >
              <MDBIcon fab icon="facebook-f" />
            </a>
           
            <a href="https://bhutanfootball.org/" className="me-4 text-reset">
              <MDBIcon fab icon="google" />
            </a>
            <a
              href="https://www.instagram.com/explore/locations/472342832788574/bhutan-football-federation/"
              className="me-4 text-reset"
            >
              <MDBIcon fab icon="instagram" />
            </a>
            
           
          </div>
        </section>

        <section className="">
          <MDBContainer className="text-center text-md-start mt-5">
            <MDBRow className="mt-3">
              <MDBCol md="3" lg="4" xl="3" className="mx-auto mb-4">
                <h6 className="text-uppercase fw-bold mb-4">
                  <MDBIcon icon="gem" className="me-3" />
                  BTR
                </h6>
                <p>
                  The Bhutan Turf Reservation is the first digital football
                  booking platform in Bhutan.
                </p>
              </MDBCol>

              <MDBCol md="2" lg="2" xl="2" className="mx-auto mb-4">
                <h6 className="text-uppercase fw-bold mb-4">Quick Links</h6>
                <a href="https://bhutanfootball.org/" className="text-reset">
                  Football Federation
                </a>
              </MDBCol>

              <MDBCol md="3" lg="2" xl="2" className="mx-auto mb-4">
                <h6 className="text-uppercase fw-bold mb-4">
                  Visit Famous Club
                </h6>
                <p>
                  <a
                    href="https://www.facebook.com/thimphucityfc/"
                    className="text-reset"
                  >
                    Thimphu City FC
                  </a>
                </p>
                <p>
                  <a href="https://parofc.com/" className="text-reset">
                    Paro FC
                  </a>
                </p>
                <p>
                  <a
                    href="https://en.wikipedia.org/wiki/Transport_United_FC"
                    className="text-reset"
                  >
                    Transport United
                  </a>
                </p>
                <p>
                  <a
                    href="https://int.soccerway.com/teams/bhutan/takin/46376/"
                    className="text-reset"
                  >
                    FC Takin
                  </a>
                </p>
              </MDBCol>

              <MDBCol md="4" lg="3" xl="3" className="mx-auto mb-md-0 mb-4">
                <h6 className="text-uppercase fw-bold mb-4">Contact</h6>
                <p>
                  <MDBIcon icon="home" className="me-2" size="1x" />
                  Gyelposhing, Mongar, Bhutan
                </p>
                <p>
                  <MDBIcon icon="envelope" className="me-3" size="1x" />
                  Jigwang@gmail.com
                </p>
                <p>
                  <MDBIcon icon="phone" className="me-3" size="1x" /> +
                  97517334455
                </p>
                <p>
                  <MDBIcon icon="print" className="me-3" size="1x" /> +975 4
                  744227 
                </p>
              </MDBCol>
            </MDBRow>
          </MDBContainer>
        </section>

        <div
          className="text-center p-4"
          style={{ backgroundColor: "rgba(0, 0, 0, 0.05)" }}
        >
          © 2023 Copyright: Bhutan Turf Reservation
        </div>
      </MDBFooter>
    </div>
  );
}

export default Footer;
