import React from "react";
import mimage1 from "./images/mimage1.jpg";
import gyelposhing3 from "./images/gyelposhing3.jpg";
import gyelposhing2 from "./images/gyelposhing2.jpg";
import Navbar from "./Navbar";
import Footer from "./Footer";
import { Link } from "react-router-dom";
function Tashigang() {
  return (
    <div>
      <Navbar />
      <div
        id="carouselBasicExample"
        className="carousel slide carousel-fade"
        data-bs-ride="carousel"
      >
        <div className="carousel-indicators">
          <button
            type="button"
            data-bs-target="#carouselBasicExample"
            data-bs-slide-to="0"
            className="active"
            aria-current="true"
            aria-label="Slide 1"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselBasicExample"
            data-bs-slide-to="1"
            aria-label="Slide 2"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselBasicExample"
            data-bs-slide-to="2"
            aria-label="Slide 3"
          ></button>
        </div>

        <div className="carousel-inner">
          <div className="carousel-item active">
            <img
              src="https://www.sherubtse.edu.bt/wp-content/uploads/2019/04/chancellorcup_final.jpg"
              className="d-block w-100"
              alt="Sunset Over the City"
              height="800px"
              style={{ pointerEvents: "none" }}
            />
            <div className="carousel-caption d-none d-md-block">
              <h3>Tashigang</h3>
              <h4>Sherubtse Football Turf</h4>
            </div>
          </div>

          <div className="carousel-item">
            <img
              src="https://kuenselonline.com/wp-content/uploads/2022/01/football-1.jpg"
              className="d-block w-100"
              alt="Canyon at Nigh"
              height="800px"
              style={{ pointerEvents: "none" }}
            />
            <div className="carousel-caption d-none d-md-block">
              <h3>Tashigang</h3>
              <h4>Sherubtse Football Turf</h4>
            </div>
          </div>

          <div className="carousel-item">
            <img
              src="https://parofc.com/wp-content/uploads/2022/10/1_OSmOfbl_L0j-wxKruHFPPw.jpeg"
              className="d-block w-100"
              alt="Cliff Above a Stormy Sea"
              height="800px"
              style={{ pointerEvents: "none" }}
            />
            <div className="carousel-caption d-none d-md-block">
              <h3>Tashigang</h3>
              <h4>Sherubtse Football Turf</h4>
            </div>
          </div>
        </div>

        <button
          className="carousel-control-prev"
          type="button"
          data-bs-target="#carouselBasicExample"
          data-bs-slide="prev"
        >
          <span
            className="carousel-control-prev-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button
          className="carousel-control-next"
          type="button"
          data-bs-target="#carouselBasicExample"
          data-bs-slide="next"
        >
          <span
            className="carousel-control-next-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>

      <div className="py-5" style={{ marginTop: 100 }} id="venue">
        <div className="container">
          <div
            className="row animate-in-down"
            style={{
              borderRadius: "10px",
              backgroundColor: "rgba(48, 194, 244, 0.8)",
            }}
          >
            <div className="p-4 col-md-6 col-sm-12 align-self-center text-color4">
              {/* <p className="m-0">Feel comfortable, and purchase items</p> */}
              <h2>Brief History Sherubtse Football Turf</h2>
              <p className="my-4">
                On the eventful evening of August 31st, 2015, the highly
                anticipated inauguration of the floodlight transpired, graced by
                the esteemed presence of Tshering Wangdi, the President of the
                College. This 72-kilowatt floodlight, generously gifted by His
                Majesty the King, accompanied by an artificial turf, which was
                inaugurated earlier that same year, was inaugurated as well. In
                commemoration of this momentous occasion, a friendly match was
                organized between the lecturers of Sherubtse College and the
                Bhutan Power Corporation team from Kanglung.
                <br></br>
                The installation of the floodlight represents a considerable
                investment, amounting to NU. 7.9 Million, which will not only
                benefit the students and faculties of the college but also the
                community at large. This is due to the fact that the
                floodlight's maintenance is also included, making it a valuable
                addition to the college's infrastructure.
              </p>{" "}
            </div>
            <div className="p-0 col-md-6 col-sm-12">
              <div
                className="carousel slide"
                data-ride="carousel"
                id="carousel1"
              >
                <div class="carousel-inner" style={{ margin: 0, padding: 0 }}>
                  <div className="carousel-item active">
                    {" "}
                    <img
                      className="d-block img-fluid w-100"
                      src="https://www.sherubtse.edu.bt/wp-content/uploads/2019/09/BUSF.jpg"
                      alt="first slide"
                    />
                    <div className="carousel-caption">
                      <h3>Sherubtse</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="section bg-c-light" style={{ marginTop: "2px" }}>
        <div className="container">
          <div className="row">
            <div className="col-md-12 mb-4 text-center">
              <h3 className="main-heading">Payment</h3>
              <div className="underline1 mx-auto"></div>
            </div>
            <p style={{ textAlign: "center" }}>
              During the reservation,you are required to pay 100% of the turf
              charge and submit the Journal number of the transaction.You can
              cancel the reservation whenever you want,but you will only receive
              a refund of 20% of the upfront money.However,if the reservation
              cancelled before one hour,you will not receive a refund.
              <br />
              <br />
              Day Time charge(6 AM-6PM)
              <br />
              Nu.2500/-
              <br />
              Night Time charge(6 AM-6PM)
              <br />
              Nu.3000/-
            </p>
          </div>
        </div>
      </section>

      <Link
        to="/reserve/Trashigang"
        style={{
          color: "#393f81",
          textDecoration: "none",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <button
          type="button"
          className="btn btn-primary btn-lg"
          style={{
            fontSize: "15px",
            marginTop: "50px",
            backgroundColor: "#007bff",
            border: "none",
            transition: "background-color 0.5s ease",
          }}
          onMouseEnter={(e) => {
            e.target.style.backgroundColor = "#5bc0de";
          }}
          onMouseLeave={(e) => {
            e.target.style.backgroundColor = "#007bff";
          }}
        >
          CLICK HERE TO RESERVE GROUND
        </button>
      </Link>

      <Footer />
    </div>
  );
}

export default Tashigang;
