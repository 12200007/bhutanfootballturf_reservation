import React, { useState, useEffect } from "react";
import "./Reserse.css";
import { useParams, useNavigate } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Navbar from "./Navbar";
import Footer from "./Footer";
import QRcode from "./images/QRCODE.jpg";

function MongarReserve() {
  const navigate = useNavigate();
  const { dzongkhag } = useParams();
  const [data, setData] = useState({
    dzongkhag,
    email: "",
    phoneNo: "",
    date: "",
    time: "",
    jrlNo: "",
  });
  const [reservedTimes, setReservedTimes] = useState([]);

  const validatePhoneNumber = () => {
    const numericValue = data.phoneNo.replace(/\D/g, ""); // Remove non-numeric characters
    const startsWithValidPrefix =
      data.phoneNo.startsWith("77") || data.phoneNo.startsWith("17");
    const hasValidLength = numericValue.length === 8;
    return startsWithValidPrefix && hasValidLength;
  };

  async function getReservation(date) {
    setData({ ...data, time: "", date });

    try {
      const res = await fetch(
        `http://localhost:5000/turf-reservations/${dzongkhag}/${date}`
      );

      if (res.ok) {
        const json = await res.json();
        setReservedTimes(json.map((item) => item.time));
      } else {
        const errMsg = await res.text();
        toast.success(
          `Server failed to fetch booked reservations for this date! ${errMsg}`
        );
      }
    } catch (err) {
      toast.success(
        `Failed to fetch booked reservations for this date! ${err.message}`
      );
    }
  }

  async function onSubmit(event) {
    event.preventDefault();

    if (!validatePhoneNumber()) {
      toast.error("Phone number is not validated");
      return;
    }

    toast.success("Reserving");

    try {
      const res = await fetch("http://localhost:5000/generate-otp", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });

      if (res.ok) {
        res
          .json()
          .then((json) => {
            navigate(`/enter-otp/${json.req_id}`);
          })
          .catch(() => {
            toast.success("Error when parsing json data!");
          });
      } else {
        toast.success(await res.text());
      }
    } catch (err) {
      toast.success(`Failed to send reservation request! ${err.message}`);
    }
  }

  return (
    <div>
      <Navbar />

      <div class="new-container">
        <h1 class="form-title">{dzongkhag} Booking form</h1>
        <form class="booking-car" onSubmit={onSubmit}>
          <div class="main-user-info">
            <div class="user-input-box">
              <label for="CarModel">Email</label>
              <input
                type="text"
                id="email"
                name="email"
                placeholder="Enter Your Email"
                onChange={(e) => {
                  setData({ ...data, email: e.target.value });
                }}
                required
                // pattern="[A-Za-z]+-[0-9]{4}"
              />
            </div>

            <div class="user-input-box">
              <label for="Vehicle Colour">Phone Number</label>
              <input
                type="number"
                id="Phone Number"
                name="Phone Number"
                placeholder="Enter Your Phone Number"
                onChange={(e) => {
                  setData({ ...data, phoneNo: e.target.value });
                }}
                required
              />
            </div>

            <div className="user-input-box" style={{ marginTop: "50px" }}>
              <label htmlFor="startingDate">Reserve Date</label>
              <input
                type="date"
                id="Reserving Date"
                name="startingDate"
                // placeholder="Enter Start_Date"
                onChange={(e) => {
                  getReservation(e.target.value);
                }}
                required
                min={new Date().toISOString().split("T")[0]}
              />
            </div>
            <div className="user-input-box" style={{ marginTop: "50px" }}>
              <label htmlFor="startingTime">
                <span className="dot available"></span>
                &nbsp;&nbsp;Available&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span className="dot booked"></span>&nbsp;&nbsp;Booked
              </label>
              <select
                id="startingTime"
                name="startingTime"
                required
                style={{
                  border: "1px solid grey",
                  height: "40px",
                  width: "95%",
                }}
                onChange={(e) => {
                  setData({ ...data, time: e.target.value });
                }}
                value={data.time}
              >
                <option value="">Select Booking Time</option>
                <option
                  value="6:00 AM - 8:00 AM"
                  disabled={reservedTimes.includes("6:00 AM - 8:00 AM")}
                  style={{
                    color: reservedTimes.includes("6:00 AM - 8:00 AM")
                      ? "red"
                      : "black",
                  }}
                >
                  {reservedTimes.includes("6:00 AM - 8:00 AM") && <span></span>}{" "}
                  6:00 AM - 8:00 AM
                </option>

                <option
                  value="8:00 AM – 10:00 AM"
                  disabled={reservedTimes.includes("8:00 AM – 10:00 AM")}
                  style={{
                    color: reservedTimes.includes("8:00 AM – 10:00 AM")
                      ? "red"
                      : "black",
                  }}
                >
                  {reservedTimes.includes("8:00 AM – 10:00 AM") && (
                    <span></span>
                  )}{" "}
                  8:00 AM – 10:00 AM
                </option>

                <option
                  value="10:00 AM – 12:00 PM"
                  disabled={reservedTimes.includes("10:00 AM – 12:00 PM")}
                  style={{
                    color: reservedTimes.includes("10:00 AM – 12:00 PM")
                      ? "red"
                      : "black",
                  }}
                >
                  {reservedTimes.includes("10:00 AM – 12:00 PM") && (
                    <span></span>
                  )}{" "}
                  10:00 AM – 12:00 PM
                </option>

                <option
                  value="12:00 PM – 2:00 PM"
                  disabled={reservedTimes.includes("12:00 PM – 2:00 PM")}
                  style={{
                    color: reservedTimes.includes("12:00 PM – 2:00 PM")
                      ? "red"
                      : "black",
                  }}
                >
                  {reservedTimes.includes("12:00 PM – 2:00 PM") && (
                    <span></span>
                  )}{" "}
                  12:00 PM – 2:00 PM
                </option>

                <option
                  value="2:00 PM – 4:00 PM"
                  disabled={reservedTimes.includes("2:00 PM – 4:00 PM")}
                  style={{
                    color: reservedTimes.includes("2:00 PM – 4:00 PM")
                      ? "red"
                      : "black",
                  }}
                >
                  {reservedTimes.includes("2:00 PM – 4:00 PM") && <span></span>}{" "}
                  2:00 PM – 4:00 PM
                </option>

                <option
                  value="4:00 PM – 6:00 PM"
                  disabled={reservedTimes.includes("4:00 PM – 6:00 PM")}
                  style={{
                    color: reservedTimes.includes("4:00 PM – 6:00 PM")
                      ? "red"
                      : "black",
                  }}
                >
                  {reservedTimes.includes("4:00 PM – 6:00 PM") && <span></span>}{" "}
                  4:00 PM – 6:00 PM
                </option>

                <option
                  value="6:00 PM – 8:00 PM"
                  disabled={reservedTimes.includes("6:00 PM – 8:00 PM")}
                  style={{
                    color: reservedTimes.includes("6:00 PM – 8:00 PM")
                      ? "red"
                      : "black",
                  }}
                >
                  {reservedTimes.includes("6:00 PM – 8:00 PM") && <span></span>}{" "}
                  6:00 PM – 8:00 PM
                </option>

                <option
                  value="8:00 PM – 10:00 PM"
                  disabled={reservedTimes.includes("8:00 PM – 10:00 PM")}
                  style={{
                    color: reservedTimes.includes("8:00 PM – 10:00 PM")
                      ? "red"
                      : "black",
                  }}
                >
                  {reservedTimes.includes("8:00 PM – 10:00 PM") && (
                    <span></span>
                  )}{" "}
                  8:00 PM – 10:00 PM
                </option>

                <option
                  value="10:00 PM – 12:00 PM"
                  disabled={reservedTimes.includes("10:00 PM – 12:00 PM")}
                  style={{
                    color: reservedTimes.includes("10:00 PM – 12:00 PM")
                      ? "red"
                      : "black",
                  }}
                >
                  {reservedTimes.includes("10:00 PM – 12:00 PM") && (
                    <span></span>
                  )}{" "}
                  10:00 PM – 12:00 PM
                </option>
              </select>
            </div>
            <div className="user-input-box" style={{ marginTop: "50px" }}>
              <label htmlFor="Scan To Pay">Scan To Pay</label>
              <img
                src={QRcode}
                alt="QRCODE"
                height="150px"
                style={{ pointerEvents: "none", marginLeft: "-15px" }}
              />
            </div>

            <div className="user-input-box" style={{ marginTop: "70px" }}>
              <label htmlFor="Journal">Journal Number</label>
              <input
                type="number"
                id="Journal Number"
                name="CarModel"
                placeholder="Enter Your Journal Number"
                style={{ marginTop: "-50px" }}
                onChange={(e) => {
                  setData({ ...data, jrlNo: e.target.value });
                }}
                required
              />
            </div>
          </div>

          <div className="form-submit-btn">
            <input
              type="submit"
              id="bookingRegistrationButton"
              value="Reserve"
            />
          </div>

          <div className="form-submit-btn2">
            <input
              type="button"
              onClick={() => navigate(-1)}
              id="bookingRegistrationButton"
              value="Cancel"
            />
          </div>
        </form>
      </div>

      <Footer />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
      <ToastContainer />
    </div>
  );
}

export default MongarReserve;
