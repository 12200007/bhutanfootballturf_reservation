import React from "react";
// import "./body4.css";

function Body4() {
  return (
    <div>
      <div className="py-5" style={{ marginTop: 0 }} id="venue">
        <div className="py-5" style={{ marginTop: 100 }} id="venue">
          <div className="container">
            <div className="row animate-in-down"  style={{borderRadius:"10px",backgroundColor:"rgba(48, 194, 244, 0.8)"}}>
              <div className="p-4 col-md-6 col-sm-12 align-self-center text-color4">
                {/* <p className="m-0">Feel comfortable, and purchase items</p> */}
                <h2>Bhutan Football Federation</h2>
                <p className="my-4" style={{ textAlign: "justify", textJustify: "inter-word" }}>
                  The Bhutan Football Federation (BFF) is the governing body of
                  football in Bhutan, controlling the Bhutan national football
                  team, Bhutan women’s national football team and the national
                  league1. It has been a member of FIFA since 2000.
                  <br></br>
                  HRH Prince Jigyel Ugyen Wangchuck is the President of the
                  Bhutan Olympic Committee and the head of sports in Bhutan.
                </p>{" "}
          
              </div>
              <div className="p-0 col-md-6 col-sm-12">
                <div
                  className="carousel slide"
                  data-ride="carousel"
                  id="carousel1"
                >
                  <div className="carousel-inner" role="listbox">
                    <div className="carousel-item active">
                      {" "}
                      <img
                        className="d-block img-fluid w-100"
                        src="https://bhutanfootball.org/wp-content/uploads/2022/07/New-academy-offers-fresh-opportunities-in-the-Land-of-the-Thunder-Dragon-850x580.jpg"
                        alt="first slide"
                      />
                      <div className="carousel-caption">
                        <h3>Bhutan Football Federation</h3>
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
     </div>
  );
}
export default Body4;
