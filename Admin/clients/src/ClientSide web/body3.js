import React from "react";
import "./body3.css";
function Body3() {
  return (
    <div
      class="container3 d-flex align-items-center justify-content-center position-relative flex-wrap"
      style={{ marginTop: "100px" }}
    >
      <div className="col-md-12 mb-4 text-center">
        <h3 className="main-heading">BEAUTIFUL FOOTBALL EVENTS IN BHUTAN</h3>
        <div className="underline3 mx-auto"></div>
      </div>
      <div class="card d-flex position-relative flex-column">
        <div class="imgContainer">
          <img src="https://bhutanfootball.org/wp-content/uploads/2022/07/BPL-Bhutan-1536x1024.jpg" />
        </div>
        <div class="content">
          <h2>Bhutan Premier league</h2>
          <p>
            Paro FC was crowned the champions of the 2021 BoB Bhutan Premier
            League (BPL) after defeating Thimphu City FC 2-1 at the
            Changlimithang Stadium yesterday in a highly contested game.
          </p>
        </div>
      </div>
      <div class="card d-flex position-relative flex-column">
        <div class="imgContainer">
          <img src="https://kuenselonline.com/wp-content/uploads/2022/10/ChukhaCS.jpg" />
        </div>
        <div class="content">
          <h2>BHSSFC 2022 </h2>
          <p>
            Bhutan Higher Secondary School Football Championship is a football
            competition among the higher schools in Bhutan, and Ugyen Academy
            won the first-ever championship.
          </p>
        </div>
      </div>
      <div class="card d-flex position-relative flex-column">
        <div class="imgContainer">
          <img src="https://i0.wp.com/c2.staticflickr.com/4/3942/18870543312_5d490f2758_o.jpg?resize=1200%2C813&ssl=1" />
        </div>
        <div class="content">
          <h2>Bhutan Versus China</h2>
          <p>
            In 2018, Bhutan played a football match against China for the FIFA
            World Cup Qualifiers for the first time in Changlimithang. However,
            Bhutan was defeated by China.
          </p>
        </div>
      </div>
    </div>
  );
}

export default Body3;
