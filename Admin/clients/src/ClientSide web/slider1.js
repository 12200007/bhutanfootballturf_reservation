import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import "./Slider.css";


function Slider() {
  return (
    <div>
      <h1 className="text1">Bhutan Football Turf Reservation </h1>
      <div className="container">
        <div className="containerSlider">
          <div className="carouselSlider">
            <figure>
              <img src="https://bhutanfootball.org/wp-content/uploads/2022/07/BPL-Bhutan-1536x1024.jpg" />
            </figure>
            <figure>
              <img src=" https://bhutanfootball.org/wp-content/uploads/2022/07/21273341_1568516863171160_3155751541999151457_o-1536x1025.jpg" />
            </figure>
            <figure>
              <img src="https://i0.wp.com/c2.staticflickr.com/4/3865/18690516899_280f0b4423_o.jpg?resize=1200%2C800&ssl=1" />
            </figure>
            <figure>
              <img src="https://bhutanfootball.org/wp-content/uploads/2022/07/Bhutan-Womens-Football-Team8-1536x1024.jpg" />
            </figure>
            <figure>
              <img src="https://bhutanfootball.org/wp-content/uploads/2022/07/BFF-Academy-boys-1536x1024.jpg" />
            </figure>
            <figure>
              <img src="https://bhutanfootball.org/wp-content/uploads/2022/07/BFF1-1536x1024.jpg" />
            </figure>
            <figure>
              <img src="https://i0.wp.com/c2.staticflickr.com/6/5348/18688010570_d56362a7df_o.jpg?resize=1200%2C800&ssl=1" />
            </figure>
            <figure>
              <img src="https://i0.wp.com/c2.staticflickr.com/6/5443/18688032450_8a8bc1dea0_o.jpg?resize=1200%2C800&ssl=1" />
            </figure>
            <figure>
              <img src="https://bhutanfootball.org/wp-content/uploads/2022/07/BFF-Academy-boys-1536x1024.jpg" />
            </figure>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Slider;
