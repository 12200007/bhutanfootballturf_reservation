import React from "react";
import "./container1.css";
import Slider from "./slider1";

function Container1() {
  return (
    <div className="container">
      <div className="row" style={{ marginTop: "60px" }}>
        <div className="col-lg-6 snip1527">
          <p style={{ textAlign: "justify", textJustify: "inter-word" }}>
            Football is a sport with a comparatively brief history in Bhutan,
            having had an initial period of favour in the mid-twentieth century,
            when it was first introduced to the country by visiting teachers
            from India and Europe. It has only achieved significantly renewed
            popularity in the early 2000s, following the advent of satellite
            television broadcasting, with historically national sport being
            archery. Consequently, the domestic game was underdeveloped. After
            the establishment of an initial league in the late 1980s, little in
            the way of recorded competition took place until the mid-1990s when
            a formal championship, the A-Division, was created. Football became
            the most popular sport in Bhutan.
          </p>
          <p style={{ textAlign: "justify", textJustify: "inter-word" }}>
            However, although this was nominally a national league, it was in
            reality merely a competition for teams based in Thimphu. This
            championship developed into its current three-tiered format, but
            remained resolutely Thimphu-focused until an annual, true national
            competition, Bhutan Premier League, was established in 2012 (as
            National League). Unfortunately, due to financial and transportation
            issues, this competition has only added at most another three teams
            to the overall number competing. Throughout the history of football
            in Bhutan, its clubs have remained in the third tier of continental
            competition within the Asian Football Confederation (AFC), the AFC
            President's Cup, and have more often than not struggled to compete
            in this competition.
          </p>
        </div>
        <div className="col-lg-6 snip1528">
          <Slider />
        </div>
      </div>
    </div>
  );
}

export default Container1;
