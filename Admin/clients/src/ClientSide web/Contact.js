import React, { useState } from "react";
import Navbar from "./Navbar.js";
import Footer from "./Footer";
import "./Contact.css";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function Contact() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [feedback, setFeedback] = useState("");
  const [submitMessage, setSubmitMessage] = useState("");

  const validatePhoneNumber = () => {
    const numericValue = phoneNumber.replace(/\D/g, ""); // Remove non-numeric characters
    const startsWithValidPrefix =
      phoneNumber.startsWith("77") || phoneNumber.startsWith("17");
    const hasValidLength = numericValue.length === 8;
    return startsWithValidPrefix && hasValidLength;
  };

  const validateEmail = () => {
    return email.endsWith("@gmail.com");
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    if (!validateEmail()) {
      // Email is not valid, display an error message or take appropriate action
      console.log("Invalid email:", email);
      toast.error("Email is invalid");
      return;
    }

    if (!validatePhoneNumber()) {
      // Phone number is not valid, display an error message or take appropriate action
      console.log("Invalid phone number:", phoneNumber);

      toast.error("Phone number is not validated");
      return;
    }

    try {
      const response = await fetch("http://localhost:5000/feedback", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ name, email, phoneNumber, feedback }),
      });

      if (response.ok) {
        const data = await response.json();
        console.log(data.message); // Feedback submitted successfully
        setSubmitMessage("Submitted successfully");

        toast.success("Feedback submitted successfully"); // Notify the user with a success message
      } else {
        console.error("Error submitting feedback:", response.status);
        setSubmitMessage("Submission failed");
        // Handle the error case, e.g., show an error message to the user
      }
    } catch (error) {
      console.error("Error submitting feedback:", error);
      setSubmitMessage("Submission failed");
      // Handle any network or other errors that may occur during the request
    }
  };

  return (
    <div>
      <Navbar />
      <section className="body1">
        <div className="contactNav">
          <div className="container mt-xl-5 mb-5">
            <div className="d-flex justify-content-center pt-5">
              <h2 className="font-weight-bold">Get in touch!</h2>
            </div>
            <div className="d-flex justify-content-center text-muted">
              Contact us for a quote or help to join the team.
            </div>
            <div className="d-md-flex flex-md-row justify-content-center py-4">
              <div className="d-md-flex flex-md-column contact px-4">
                <div className="d-md-flex justify-content-center icon py-2">
                  {" "}
                  <span className="fa fa-map-marker"></span>{" "}
                  <span className="mobile-info text-dark p-2 pb-3">
                    Gyelposhing, Bhutan
                  </span>{" "}
                </div>
                <div className="contact-info">Gyelposhing, Bhutan</div>
              </div>
              <div className="d-flex flex-column contact px-4">
                <div className="d-md-flex justify-content-center icon py-2">
                  {" "}
                  <span className="fa fa-phone"></span>{" "}
                  <span className="mobile-info text-dark p-2 pb-3">
                    +97517334455
                  </span>{" "}
                </div>
                <div className="contact-info">+97517334455</div>
              </div>
              <div className="d-flex flex-column contact px-4">
                <div className="d-md-flex justify-content-center icon py-2">
                  {" "}
                  <span className="fa fa-envelope"></span>{" "}
                  <span className="mobile-info text-dark p-2 pb-3">
                    Jigwang@gmail.com
                  </span>{" "}
                </div>
                <div className="contact-info">Jigwang@gmail.com</div>
              </div>
            </div>
            <div className="d-flex flex-row justify-content-center">
              <form className="w-xl-50 w-lg-75" onSubmit={handleSubmit}>
                <div className="container">
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label htmlFor="name">Name</label>
                        <div className="input-field">
                          <span className="fa fa-user-o p-2 border-right"></span>
                          <input
                            type="text"
                            onChange={(e) => setName(e.target.value)}
                            required
                          />
                        </div>
                      </div>
                      <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <div className="input-field">
                          <span className="fa fa-envelope-o p-2"></span>{" "}
                          <input
                            type="email"
                            onChange={(e) => setEmail(e.target.value)}
                            required
                          />
                        </div>
                      </div>
                      <div className="form-group">
                        <label htmlFor="phone">Phone</label>
                        <div className="input-field">
                          <span className="fa fa-mobile p-2"></span>
                          <input
                            type="text"
                            onChange={(e) => {
                              const numericValue = e.target.value.replace(
                                /\D/g,
                                ""
                              ); // Remove non-numeric characters
                              setPhoneNumber(numericValue);
                            }}
                            onKeyDown={(e) => {
                              if (
                                isNaN(parseInt(e.key, 10)) &&
                                e.key !== "Backspace"
                              ) {
                                e.preventDefault(); // Prevent non-numeric characters from being entered
                              }
                            }}
                            required
                          />
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <label htmlFor="feedback">Feedback</label>
                        <div className="input-field bg-light">
                          <textarea
                            name="feedback"
                            id="feedback"
                            cols="10"
                            rows="9"
                            className="form-control bg-light"
                            placeholder="Your Feedback"
                            onChange={(e) => setFeedback(e.target.value)}
                            required
                          ></textarea>{" "}
                        </div>
                      </div>
                    </div>
                    <div className="d-flex flex-row justify-content-center w-100">
                      <input
                        type="submit"
                        name="submit"
                        value="Submit"
                        className="btn"
                        style={{ color: "black", backgroundColor: "lightgray" }}
                      />
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
      <Footer />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
      <ToastContainer />
    </div>
  );
}

export default Contact;
