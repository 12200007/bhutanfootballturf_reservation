import { Link } from "react-router-dom";
import { useState } from "react"; // Import useState hook
import "./Navbar.css";
import logo from "./images/logo.jpeg";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { faHouse } from "@fortawesome/free-solid-svg-icons";
import { faAddressBook } from "@fortawesome/free-solid-svg-icons";
import { faCircleInfo } from "@fortawesome/free-solid-svg-icons";
import { faLandmark } from "@fortawesome/free-solid-svg-icons";

function Navbar() {
  const [expanded, setExpanded] = useState(false); // State to track the expanded/collapsed state

  const handleToggle = () => {
    setExpanded(!expanded); // Toggle the expanded state
  };

  return (
    <nav
      className="navbar navbar-expand-lg bg-light sticky-top"
      style={{ borderRadius: "5px" }}
    >
      <div className="container-fluid">
        <img src={logo} className="logo" alt="Services" />
        <button
          className={`navbar-toggler ${expanded ? "collapsed" : ""}`}
          type="button"
          onClick={handleToggle} // Add onClick event handler
          aria-controls="navbarSupportedContent"
          aria-expanded={expanded ? "true" : "false"}
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div
          className={`collapse navbar-collapse ${expanded ? "show" : ""}`}
          id="navbarSupportedContent"
        >
          <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <Link to="/" className="nav-link active">
                <FontAwesomeIcon icon={faHouse} /> Home
              </Link>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="dzongkhagDropdown"
                role="button"
                data-bs-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                <FontAwesomeIcon
                  icon={faLandmark}
                  style={{ color: "black", marginRight: "5px" }}
                />
                <span style={{ fontWeight: "bold", color: "black" }}>
                  Dzongkhag
                </span>
              </a>
              <div
                className="dropdown-menu"
                aria-labelledby="dzongkhagDropdown"
              >
                <Link className="dropdown-item" to="/mongar">
                  Mongar
                </Link>
                <Link className="dropdown-item" to="/SamdrupJongkhar">
                  Samdrup Jongkhar
                </Link>
                <Link className="dropdown-item" to="/tashigang">
                  Tashigang
                </Link>
              </div>
            </li>

            <li className="nav-item">
              <Link to="/contact" className="nav-link active">
                <FontAwesomeIcon icon={faAddressBook} /> Contact Us
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/about" className="nav-link active">
                <FontAwesomeIcon icon={faCircleInfo} />
                About Us
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
