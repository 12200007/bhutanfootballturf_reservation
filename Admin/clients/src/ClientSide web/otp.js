import React, { useState } from "react";
import "./Reserse.css";
import { useParams, useNavigate } from "react-router-dom";
import { toast,ToastContainer } from "react-toastify";
import Navbar from "./Navbar";
import Footer from "./Footer";
function Otp() {
  const navigate = useNavigate();
  const { reqId } = useParams();
  const [data, setData] = useState({ enteredOtp: "", reqId });

  async function onSubmit(event) {
    event.preventDefault();

    try {
      const res = await fetch("http://localhost:5000/turf-reservation", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });

      if (res.ok) {
       
        toast.success("Reservation Successfully");
      } else {
        // alert(await res.text());
        toast.error(await res.text());
      }
    } catch (err) {
      // alert(err.message);
      toast.error(err.message);
    }
  }

  return (
    <div>
      <Navbar />

      <div class="new-container">
        <h1 class="form-title">Booking verification</h1>
        <form class="booking-car" onSubmit={onSubmit}>
          <div class="main-user-info">
            <div className="user-input-box" style={{ marginTop: "50px" }}>
              <label htmlFor="Journal">Enter OTP</label>
              <input
                type="number"
                id="Journal Number"
                name="CarModel"
                placeholder="__ __ __ __ __ __"
                onChange={(e) => {
                  setData({ ...data, enteredOtp: e.target.value });
                }}
                required
              />
            </div>
          </div>

          <div className="form-submit-btn">
            <input
              type="submit"
              id="bookingRegistrationButton"
              value="Submit"
            />
          </div>

          <div className="form-submit-btn2">
            <input
              type="button"
              onClick={() => navigate(-1)}
              id="bookingRegistrationButton"
              value="Cancel"
            />
          </div>
        </form>
      </div>

      <Footer />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
     
      <ToastContainer />
    </div>
  );
}

export default Otp;
