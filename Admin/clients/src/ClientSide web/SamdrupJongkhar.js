import React from "react";
import mimage1 from "./images/mimage1.jpg";
// import gyelposhing3 from "./images/gyelposhing3.jpg";
// import gyelposhing2 from "./images/gyelposhing2.jpg";
import Navbar from "./Navbar";
import Footer from "./Footer";
import { Link } from "react-router-dom";
function SamdrupJongkhar() {
  return (
    <div>
      <Navbar />
      <div
        id="carouselBasicExample"
        className="carousel slide carousel-fade"
        data-bs-ride="carousel"
      >
        <div className="carousel-indicators">
          <button
            type="button"
            data-bs-target="#carouselBasicExample"
            data-bs-slide-to="0"
            className="active"
            aria-current="true"
            aria-label="Slide 1"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselBasicExample"
            data-bs-slide-to="1"
            aria-label="Slide 2"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselBasicExample"
            data-bs-slide-to="2"
            aria-label="Slide 3"
          ></button>
        </div>

        <div className="carousel-inner">
          <div className="carousel-item active">
            <img
              src="https://mlmbqcss2lv7.i.optimole.com/w:auto/h:auto/q:90/f:avif/https://www.sispitches.com/wp-content/uploads/2016/11/football.jpg"
              className="d-block w-100"
              alt="Sunset Over the City"
              height="800px"
              style={{ pointerEvents: "none" }}
            />
            <div className="carousel-caption d-none d-md-block">
              <h3>Samdrup Jongkhar</h3>
              <h4>Samdrup Jongkhar Football Turf</h4>
            </div>
          </div>

          <div className="carousel-item">
            <img
              src="https://www.dailybhutan.com/pub_files/00010303937847/contributing-to-the-country-through-the-love-for-football_3233.jpg"
              className="d-block w-100"
              alt="Canyon at Nigh"
              height="800px"
              style={{ pointerEvents: "none" }}
            />
            <div className="carousel-caption d-none d-md-block">
              <h3>Samdrup Jongkhar</h3>
              <h4>Samdrup Jongkhar Football Turf</h4>
            </div>
          </div>

          <div className="carousel-item">
            <img
              src="https://attackingsoccer.com/wp-content/uploads/2021/02/The-Reason-Why-All-Soccer-Players-Prefer-Grass-Over-Turf.jpg"
              className="d-block w-100"
              alt="Cliff Above a Stormy Sea"
              height="800px"
              style={{ pointerEvents: "none" }}
            />
            <div className="carousel-caption d-none d-md-block">
              <h3>Samdrup Jongkhar</h3>
              <h4>Samdrup JongkharFootball Turf</h4>
            </div>
          </div>
        </div>

        <button
          className="carousel-control-prev"
          type="button"
          data-bs-target="#carouselBasicExample"
          data-bs-slide="prev"
        >
          <span
            className="carousel-control-prev-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button
          className="carousel-control-next"
          type="button"
          data-bs-target="#carouselBasicExample"
          data-bs-slide="next"
        >
          <span
            className="carousel-control-next-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>

      <div className="py-5" style={{ marginTop: 100 }} id="venue">
        <div className="container">
          <div
            className="row animate-in-down"
            style={{
              borderRadius: "10px",
              backgroundColor: "rgba(48, 194, 244, 0.8)",
            }}
          >
            <div className="p-4 col-md-6 col-sm-12 align-self-center text-color4">
              {/* <p className="m-0">Feel comfortable, and purchase items</p> */}
              <h2>Brief History of SamdrupJongkhar Football Turf</h2>
              <p className="my-4">
                The Bhutan Olympic Committee (BOC) has invested an estimated
                amount of Nu 26 million to install an advanced artificial turf
                in the town of Samdrup Jongkhar, thereby making it the second
                dzongkhag in the eastern region of Bhutan to offer such a modern
                sporting facility. It is noteworthy that the first artificial
                turf ground in the east is already operational in Gyalpozhing,
                Mongar.
                <br></br>
                The installation of this ground will have a significant impact
                on the community, offering them a place to indulge in various
                sports and activities. Furthermore, students of Samdrup Jongkhar
                will have access to this state-of-the-art facility, providing
                them with ample opportunities to hone their skills in various
                sports. This facility is expected to improve the overall quality
                of sports in the region.
              </p>{" "}
            </div>
            <div className="p-0 col-md-6 col-sm-12">
              <div
                className="carousel slide"
                data-ride="carousel"
                id="carousel1"
              >
                <div class="carousel-inner" style={{ margin: 0, padding: 0 }}>
                  <div className="carousel-item active">
                    {" "}
                    <img
                      className="d-block img-fluid w-100"
                      src="https://bhutanfootball.org/wp-content/uploads/2022/07/BFF-Academy-boys-1536x1024.jpg"
                      alt="first slide"
                    />
                    <div className="carousel-caption">
                      <h3>Samdrup Jongkhar</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="section bg-c-light" style={{ marginTop: "2px" }}>
        <div className="container">
          <div className="row">
            <div className="col-md-12 mb-4 text-center">
              <h3 className="main-heading">Payment</h3>
              <div className="underline1 mx-auto"></div>
            </div>
            <p style={{ textAlign: "center" }}>
              During the reservation,you are required to pay 100% of the turf
              charge and submit the Journal number of the transaction.You can
              cancel the reservation whenever you want,but you will only receive
              a refund of 20% of the upfront money.However,if the reservation
              cancelled before one hour,you will not receive a refund.
              <br />
              <br />
              Day Time charge(6 AM-6PM)
              <br />
              Nu.2500/-
              <br />
              Night Time charge(6 AM-6PM)
              <br />
              Nu.3000/-
            </p>
          </div>
        </div>
      </section>

      <Link
        to="/reserve/Samdrup Jongkhar"
        style={{
          color: "#393f81",
          textDecoration: "none",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <button
          type="button"
          className="btn btn-primary btn-lg"
          style={{
            fontSize: "15px",
            marginTop: "50px",
            backgroundColor: "#007bff",
            border: "none",
            transition: "background-color 0.5s ease",
          }}
          onMouseEnter={(e) => {
            e.target.style.backgroundColor = "#5bc0de";
          }}
          onMouseLeave={(e) => {
            e.target.style.backgroundColor = "#007bff";
          }}
        >
          CLICK HERE TO RESERVE GROUND
        </button>
      </Link>

      <Footer />
    </div>
  );
}

export default SamdrupJongkhar;
