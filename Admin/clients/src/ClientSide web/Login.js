import {
  MDBBtn,
  MDBContainer,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBRow,
  MDBCol,
  MDBIcon,
  MDBInput,
} from "mdb-react-ui-kit";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "mdb-react-ui-kit/dist/css/mdb.min.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "./Navbar";
import Footer from "./Footer";
import React, { Fragment, useEffect, useState } from "react";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Login = ({ setAuth, authenticatedUser }) => {
  const [inputs, setInputs] = useState({
    email: "",
    password: "",
  });

  const { email, password } = inputs;

  // const n = () => {
  //   toast("ok");
  //   console.log("okok");
  // };
  const onChange = (e) =>
    setInputs({ ...inputs, [e.target.name]: e.target.value });
  const onSubmitForm = async (e) => {
    e.preventDefault();

    try {
      const body = { email, password };
      const response = await fetch("http://localhost:5000/auth/login", {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify(body),
      });

      const parseRes = await response.json();
      console.log(parseRes);
      if (parseRes.token) {
        console.log("ok");
        localStorage.setItem("token", parseRes.token);
        setAuth(true);
        toast.success("Login successful!");
        alert("Login successful!");
      } else {
        console.log("not ok");
        setAuth(false);
        toast.error(parseRes);
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  return (
    <div>
      <Navbar />
      <MDBContainer
        className="my-10"
        style={{ marginTop: "30px", width: "50%", height: "80vh" }}
      >
        <MDBCard>
          <MDBRow className="g-0">
            <MDBCol md="6">
              <MDBCardImage
                src="https://img.favpng.com/5/15/6/football-player-drawing-sport-clip-art-png-favpng-TncvSuM0A7DzY7MwJYa6NcJsC.jpg"
                alt="login form"
                className="rounded-start"
                style={{
                  maxWidth: "100%",
                  height: "100%",
                  pointerEvents: "none",
                }}
              />
            </MDBCol>

            <MDBCol md="6">
              <MDBCardBody className="d-flex flex-column">
                <div className="d-flex flex-row mt-2">
                  <MDBIcon
                    fas
                    icon="cubes fa-3x me-3"
                    style={{ color: "#ff6219" }}
                  />
                  <span className="h1 fw-bold mb-0">BTR</span>
                </div>

                <h5
                  className="fw-normal my-4 pb-3"
                  style={{ letterSpacing: "1px" }}
                >
                  Sign into your account
                </h5>
                <form onSubmit={onSubmitForm}>
                  <MDBInput
                    wrapperClass="mb-4"
                    label="Email address"
                    id="formControlLg"
                    type="text"
                    name="email"
                    value={email}
                    onChange={(e) => onChange(e)}
                    size="lg"
                  />
                  <MDBInput
                    wrapperClass="mb-4"
                    label="Password"
                    id="formControlLg"
                    type="password"
                    name="password"
                    value={password}
                    onChange={(e) => onChange(e)}
                    size="lg"
                  />

                  <MDBBtn
                    href="/homelogin"
                    className="mx-auto mx-md-0 px-md-5 py-3 py-md-2 d-block w-100"
                    color="primary"
                    size="lg"
                    type="submit"
                  >
                    Login
                  </MDBBtn>
                </form>
                <a className="small text-muted" href="#!">
                  Forgot password?
                </a>
                <p className="mb-5 pb-lg-2" style={{ color: "#393f81" }}>
                  Don't have an account?{" "}
                  <a href="/register" style={{ color: "#393f81" }}>
                    Sign Up
                  </a>
                </p>
              </MDBCardBody>
            </MDBCol>
          </MDBRow>
        </MDBCard>
      </MDBContainer>
      <Footer />
    </div>
  );
};

export default Login;
