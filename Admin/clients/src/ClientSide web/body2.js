import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEyeLowVision, faLightbulb, faHandsHolding } from '@fortawesome/free-solid-svg-icons';

import "./body2.css";

function Body2() {
  return (
    <>
      <div className="social-box">
        <div className="container">
          <div className="row">
            <div className="col-lg-4 col-xs-12 text-center">
              <div className="box">
                <FontAwesomeIcon icon={faEyeLowVision} className="fa-3x" />
                <div className="box-title">
                  <h3>Vision</h3>
                </div>
                <div className="box-text">
                  <span>
                    To provide an innovative and user-friendly platform that
                    enables football enthusiasts to easily book football pitches
                    and venues online.
                  </span>
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-xs-12 text-center">
              <div className="box">
                <FontAwesomeIcon icon={faLightbulb} className="fa-3x" />
                <div className="box-title">
                  <h3>Mission</h3>
                </div>
                <div className="box-text">
                  <span>
                    To provide a convenient, reliable, and seamless booking
                    experience for football enthusiasts who want to book a
                    football pitch or venue online.
                  </span>
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-xs-12 text-center">
              <div className="box">
                <FontAwesomeIcon icon={faHandsHolding} className="fa-3x" />
                <div className="box-title">
                  <h3>Core Values</h3>
                </div>
                <div className="box-text">
                  <span>
                    To provide a user-friendly platform, accurate information and
                    confirmed bookings, excellent support, continuous
                    improvement, transparent pricing and conditions.
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Body2;
