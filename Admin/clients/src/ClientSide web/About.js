import React from "react";
import Navbar from "./Navbar";
import Footer from "./Footer";
import "./About.css";

function About() {
  return (
    <div>
      <Navbar />
      <h3 style={{ textAlign: "center" }}>
        <strong>About Us</strong>
      </h3>

      <div
        className="pyyy-5"
        style={{
          marginTop: 50,
          textAlign: "center",
          marginLeft: 60,
          boxShadow: "2px 2px 5px rgba(0, 0, 0, 0.25)",
          borderRadius: "10px",
        }}
      >
        <p>
          Bhutan Fooyball Turf Reservation is the first online platform in Bhutan that
          allows users to reserve grounds online. This website will help people
          all over Bhutan to reserve grounds quickly and effectively, which will
          minimize the workload for both players and turf management. Users will
          be able to book turf at any moment by checking its availability,
          without having to get in touch with the groundskeeper directly.
        </p>
      </div>

      <h2 className="tops1" style={{ textAlign: "center", marginTop: "80px" }}>
        Why Choose Us?
      </h2>

      <div
        className="container3 d-flex align-items-center justify-content-center position-relative flex-wrap"
        style={{ marginTop: "50px" }}
      >
        <div className="grid">
          <div className="col1">
            <h3 style={{ color: "black", fontWeight: 500 }}>Fast</h3>
            <p style={{ color: "white", fontWeight: 500 }}>
              Our website provides a convenient and seamless way for users to
              book sports grounds and facilities without the need to physically
              visit the location.
            </p>
          </div>
          <div className="col2">
            <h3 style={{ color: "black", fontWeight: 500 }}>Save Time</h3>
            <p style={{ color: "white", fontWeight: 500 }}>
              Our website's eliminates the need for users to physically visit a
              sports ground. This provides a number of benefits, including
              significant time savings for users.
            </p>
          </div>
          <div className="col3">
            <h3 style={{ color: "black", fontWeight: 500 }}>Quality</h3>
            <p style={{ color: "white", fontWeight: 500 }}>
              Our website is committed to providing a high-quality user
              experience, and we stand behind our products and services.
            </p>
          </div>
        </div>

        <div className="grid">
          <div className="col4">
            <h3 style={{ color: "black", fontWeight: 500 }}>
              Real-time Stadium Management
            </h3>
            <p style={{ color: "black", fontWeight: 500 }}>
              Our website serves as a comprehensive information hub for
              prospective customers seeking to inquire about the availability of
              our grounds. By accessing our website, customers are able to
              obtain real-time updates on the status of our grounds, including
              information on their reservation status, without the need to
              directly engage with the relevant ground owner.
            </p>
          </div>
          <div className="col5">
            <h3 style={{ color: "black", fontWeight: 500 }}>
              Scheduling the ground
            </h3>
            <p style={{ color: "white", fontWeight: 500 }}>
              Our website offers a seamless and convenient way for customers to
              reserve sports grounds and facilities from anywhere in the world.
              Whether you're at home, in the office, or on the go, you can
              easily search for and book available grounds using our advanced
              online booking system
            </p>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default About;
