import React from "react";
import "./body1.css";

function Body1() {
  return (
    <div>
      <section className="section bg-c-light" style={{ marginTop: "100px" }}>
        <div className="container">
          <div className="row">
            <div className="col-md-12 mb-4 text-center">
              <h3 className="main-heading">BACKGROUND</h3>
              <div className="underline1 mx-auto"></div>
            </div>
            Football was introduced to Bhutan in the 20th century by British tea
            planters. The first recorded football match in Bhutan was played in
            1973 between two teams from Thimphu and Paro.The Bhutan Football
            Federation (BFF) was established in 1983, and the national team
            played its first international match against Bangladesh in 1982.
            Bhutanese football suffered due to lack of resources and funding,
            and the national team struggled to make a mark in international
            competitions.
          </div>
        </div>
      </section>
    </div>
  );
}
export default Body1;
