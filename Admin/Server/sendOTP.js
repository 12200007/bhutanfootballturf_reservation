const nodemailer = require('nodemailer');
const randomstring = require('randomstring');

// Create a temporary email address to send the OTP
const temporaryEmail = 'yourtemporary@gmail.com';

// Create a reusable transporter object using SMTP transport
const transporter = nodemailer.createTransport({
  sendmail: true,
  newline: 'unix',
  path: '/usr/sbin/sendmail',
});

// Send OTP controller function
exports.sendOTP = async (req, res) => {
  try {
    const { email } = req.body;
    const otp = Math.floor(1000 + Math.random() * 9000); // Generate a 4-digit OTP as an integer

    const mailOptions = {
      from: temporaryEmail,
      to: email,
      subject: 'OTP Verification',
      text: `Your OTP is: ${otp}`,
    };

    await transporter.sendMail(mailOptions);
    res.json({ success: true });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, error: 'Failed to send OTP' });
  }
};
