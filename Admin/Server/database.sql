CREATE DATABASE store;

CREATE TABLE users(
  user_id uuid PRIMARY KEY DEFAULT 
  uuid_generate_v4(),
  user_name VARCHAR(255) NOT NULL,
  user_email VARCHAR(255) NOT NULL,
  user_password VARCHAR(255) NOT NULL
);

CREATE TABLE turf_reservations(
  resrv_id uuid PRIMARY KEY DEFAULT 
  uuid_generate_v4(),
  dzongkhag VARCHAR(255) NOT NULL,
  date DATE NOT NULL,
  time VARCHAR(255) NOT NULL,
  resrv_email VARCHAR(255) NOT NULL,
  resrv_phone_number INTEGER NOT NULL,
  jrl_no INTEGER NOT NULL,
  doc TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE turf_reservation_requests(
  req_id uuid PRIMARY KEY DEFAULT 
  uuid_generate_v4(),
  dzongkhag VARCHAR(255) NOT NULL,
  date DATE NOT NULL,
  time VARCHAR(255) NOT NULL,
  resrv_email VARCHAR(255) NOT NULL,
  resrv_phone_number INTEGER NOT NULL,
  jrl_no INTEGER NOT NULL,
  otp INTEGER NOT NULL,
  doc TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE forgot_password_requests(
  req_id uuid PRIMARY KEY DEFAULT 
  uuid_generate_v4(),
  email VARCHAR(255) NOT NULL,
  otp INTEGER NOT NULL,
  doc TIMESTAMP NOT NULL DEFAULT NOW()
);

 CREATE TABLE  feedback (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    phone_number VARCHAR(20) NOT NULL,
    feedback TEXT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
  );

-- CREATE TABLE todo(
--   todo_id SERIAL,
--   user_id UUID ,
--   description VARCHAR(255),
--   PRIMARY KEY (todo_id),
--   FOREIGN KEY (user_id) REFERENCES users(user_id)
-- );


-- INSERT INTO users (user_name, user_email, user_password) VALUES ('henry',
--  'henryly213@gmail.com', 'kthl8822');

--  INSERT INTO users (user_name, user_email, user_password) VALUES ('Jigme',
--  'Jigme213@gmail.com', '1234');
 