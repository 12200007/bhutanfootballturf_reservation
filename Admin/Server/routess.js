const express = require('express');
const router = express.Router();
const sendOTPController = require('./sendOTP');

// Generate and send the OTP
router.post('/send-otp', sendOTPController.sendOTP);

module.exports = router;
