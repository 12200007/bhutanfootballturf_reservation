const express = require("express");
const app = express();
const cors = require("cors");
const nodemailer = require("nodemailer");
const pool = require("./db");

app.use(express.json());
app.use(cors());

function diff_minutes(dt2, dt1) {
  const diff = dt2.getTime() - dt1.getTime();

  return diff / 60000;
}

// Generate random OTP
function generateOTP() {
  return Math.floor(100000 + Math.random() * 900000);
}

// Nodemailer configuration
// const transporter = nodemailer.createTransport();

async function sendEmail(to, subject, html) {
  let transporter = nodemailer.createTransport({
    host: "smtp.zoho.com",
    secure: true,
    port: 465,
    auth: {
      user: "bhutanfootballturf@zohomail.com",
      pass: "@wangs002",
    },
  });

  await transporter.sendMail({
    from: "bhutanfootballturf@zohomail.com",
    to: to,
    subject: subject,
    html: html,
  });
}

// Route to generate and send OTP via email
app.post("/generate-otp", async (req, res) => {
  const { dzongkhag, email, phoneNo, date, time, jrlNo } = req.body;
  const otp = generateOTP();

  try {
    await pool.query("BEGIN");
    try {
      const reqTb = await pool.query(
        "INSERT INTO turf_reservation_requests\
      (dzongkhag, resrv_email, resrv_phone_number, date, time, jrl_no, otp)\
      VALUES($1, $2, $3, $4, $5, $6, $7)\
      RETURNING *",
        [dzongkhag, email, phoneNo, date, time, jrlNo, otp]
      );
      console.log("rows", reqTb.rows);

      await sendEmail(email, "Football Booking OTP", `<p> ${otp.toString()} is your OTP and it expires in 5 minutes.</p>`);

      pool.query("COMMIT");

      return res.status(200).json(reqTb.rows[0]);
    } catch (err) {
      pool.query("ROLLBACK");

      return res.status(500).send(`Failed to send otp! ${err.message}`);
    }
  } catch (err) {
    res.status(500).send(`Failed to send otp! ${err.message}`);
  }
});

app.post("/turf-reservation", async (req, res) => {
  const { reqId, enteredOtp } = req.body;

  try {
    const reqTb = await pool.query(
      "SELECT * FROM turf_reservation_requests\
    WHERE req_id = $1",
      [reqId]
    );

    if (reqTb.rows.length === 0) {
      return res.status(403).send("Turf reservation request not found!");
    }

    const {
      dzongkhag,
      resrv_email,
      resrv_phone_number,
      date,
      time,
      jrl_no,
      otp,
      doc,
    } = reqTb.rows[0];
    const now = new Date();
    const otpDate = new Date(doc);

    if (parseInt(enteredOtp) !== otp) {
      return res.status(402).send("OTP invalid!");
    }

    if (diff_minutes(now, otpDate) > 5) {
      return res.status(498).send("OTP expired!");
    }

    const bookedResrvTb = await pool.query(
      "SELECT * FROM turf_reservations\
    WHERE dzongkhag = $1 AND date = $2 AND time = $3",
      [dzongkhag, date, time]
    );

    if (bookedResrvTb.rows.length > 0) {
      return res.status(500).send("Turf has been reservered already!");
    }

    await pool.query("BEGIN");
    try {
      await pool.query(
        "INSERT INTO turf_reservations\
      (dzongkhag, resrv_email, resrv_phone_number, date, time, jrl_no)\
      VALUES($1, $2, $3, $4, $5, $6)\
      RETURNING *",
        [dzongkhag, resrv_email, resrv_phone_number, date, time, jrl_no]
      );

      const html = `<div>
      Your Reservation Time is from : <b>${time}</b><br>
      Dzongkhag: <b>${dzongkhag}</b><br>
      Date Of Booking: <b>${date}</b><br>
      
     
      </div>`;
      await sendEmail(resrv_email, "Football Reservation details", html);

      pool.query("COMMIT");

      return res.status(200).json(reqTb.rows[0]);
    } catch (err) {
      return res.status(500).send(`Failed to reserve! ${err.message}`);
    }
  } catch (err) {
    res.status(500).send(`Failed to reserve! ${err.message}`);
  }
});

app.get("/turf-reservations/:dzongkhag/:date", async (req, res) => {
  const { dzongkhag, date } = req.params;

  try {
    const resrvTb = await pool.query(
      "SELECT time FROM turf_reservations\
    WHERE dzongkhag = $1 AND date = $2",
      [dzongkhag, date]
    );

    res.status(200).json(resrvTb.rows);
  } catch (err) {
    res.status(500).send(`Failed to get reservations! ${err.message}`);
  }
});

app.post("/feedback", async (req, res) => {
  try {
    const { name, email, phoneNumber, feedback } = req.body;

    // Insert the feedback into the database
    const query =
      "INSERT INTO feedback (name, email, phone_number, feedback) VALUES ($1, $2, $3, $4)";
    await pool.query(query, [name, email, phoneNumber, feedback]);

    res.status(200).json({ message: "Feedback submitted successfully" });
  } catch (error) {
    console.error("Error submitting feedback:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// Start the server
app.listen(5000, () => {
  console.log("Server is running on port 5000");
});
